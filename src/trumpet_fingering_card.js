'use strict';

class TrumpetFingeringCard {
  // Methods:
  constructor(canvas_id, is_mobile=false) {
    this.canvas = this._getId(canvas_id);
    this.c = this.canvas.getContext('2d');

    this.top = 20; // This is the unit size
    if (is_mobile) {
      this.top = 40;
    }
    this.left = this.top;
    this.circle_radius = this.top/3;
    this.canvas.width = this.left*4;
    this.canvas.height = this.top*7;
    this.tab = null;
  }

  //////////////// PUBLIC ////////////////////
  Draw(tab) {
    // tab: [ "101", "100" ]
    this.tab = tab;
    for (const i in tab) {
      // for each tab
      for (const j in tab[i]) {
        // for each note
        let is_full = (tab[i][j] == '1')? true : false;
        this.DrawCircle(parseInt(j)+1, parseInt(i)+1, is_full);
      }
    }
  }


  DrawText(text) {
    let c = this.c;
    c.font = this.top + 'px sans';
    let t = c.measureText(text);
    c.fillText(text, this.left*2 - t.width/2, this.top);
  };


  DrawCircle(x, y, is_full=true) {
    // Draw a circle at given coords (full or not)
    // x = 1,2,3
    // y = 1,...,6
    const c = this.c;
    c.beginPath();
    x = this.left*x;
    y = this.top*(y+1) ;
    c.arc(x, y, this.circle_radius, 0, 2 * Math.PI);
    if (is_full) {
      c.fill();
    } else {
      c.lineWidth = 3;
      c.stroke();
    }
    c.lineWidth = 1; // reset
  }


  _getId(id) {
    return document.getElementById(id);
  }
}; //class TrumpetFingeringCard
