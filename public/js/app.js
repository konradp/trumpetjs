let scale = [
  false, // F#
  true,  // G
  false, // G#
  true,  // A
  false, // A#
  true,  // B
  true,  // C
  false, // C#
  true,  // D
  false, // D#
  true,  // E
  true,  // F
];
let tmp_scale = scale;
let notes = [
  'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'
];
let cards = [];
let scale_notes_only = false;
let cur_scale = 'C';
let synth = new Tinysynth();
let is_mobile = false;
window.addEventListener('resize', checkMobile);



window.onload = function() {
  checkMobile();
  drawCards();
  showHideCards();
  drawScalePicker();

  // Scale-notes-only checkbox
  let scale_notes_picker = _getId('scale-notes-only');
  scale_notes_picker.addEventListener('change', OnScaleNotesOnlyOnOff, false);

  // Buttons and overlays
  btnSettings = _getId('settings');
  btnSettings.addEventListener('click', openSettings);
  btnSettingsClose = _getId('btn-settings-close');
  btnSettingsClose.addEventListener('click', openSettings);
  viewSettings = _getId('view-settings');
}


function drawCards() {
  const div = _getId('main');
  div.innerHTML = '';
  for (i in fingerings) {
    const c = _new('canvas');
    c.className = 'card';
    // TODO
    c.id = i;
    div.appendChild(c);
    cards.push(c);
    const card = new TrumpetFingeringCard(i, is_mobile);
    card.Draw(fingerings[i]['tab']);
    card.DrawText(fingerings[i]['note']);
    c.setAttribute('concert_note', fingerings[i]['concert']);
    c.addEventListener('mousedown', (e) => {
      let note = e.target.getAttribute('concert_note');
      synth.noteOn(note, duration=500);
    });
  }

}


function checkMobile() {
  let before = is_mobile;
  if (navigator.userAgent.includes('Mobile')
    || 'ontouchstart' in document.documentElement
  ) {
    is_mobile = true;
  } else {
    is_mobile = false;
  }
  if (is_mobile != before) {
    drawCards();
    showHideCards();
  }
};


function GetNotePositions(note) {
  // e.g. F# = 0, 12, 24. 30 in total
  let tmp = [...notes];
  for (let i = 1; i <= 6; i++) {
    // Move last element to first
    let popped = tmp.pop();
    tmp.unshift(popped);
  }
  let idx = tmp.indexOf(note);
  return idx;
}


function showHideCards() {
  let root_positions = GetNotePositions(cur_scale);
  for (let i in cards) {
    let j = i % scale.length;
    cards[i].style = 'display: inline';
    if (!tmp_scale[j]) {
      // not in scale: grey or hide
      if (scale_notes_only) {
        cards[i].style = 'display: none; color: lightgrey';
      }
    } else {
      cards[i].style = 'display: inline; background-color: lightgray';
    }
    if (j == root_positions) {
      cards[i].style = 'display: inline; background-color: lightgreen';
    }
  }
}

function OnScaleNotesOnlyOnOff(event) {
  // On/off
  scale_notes_only = event.target.checked;
  showHideCards();
}


function drawScalePicker() {
  let picker = _getId('scales');
  for (i in notes) {
    let opt = _new('option');
    opt.innerHTML = notes[i];
    picker.appendChild(opt);
  }
  picker.addEventListener('change', OnChangeScale, false);
}


function OnChangeScale(event) {
  let scale_offset = notes.indexOf(event.target.value);
  tmp_scale = [...scale];
  cur_scale = event.target.value;
  for (let i = 1; i<= scale_offset; i++) {
    // move last element to first
    let popped = tmp_scale.pop();
    tmp_scale.unshift(popped);
  }
  showHideCards();
  // Update concert scale
  let concert_scale = _getId('concert-scale');
  let new_idx = (notes.indexOf(cur_scale) + 10) % notes.length;
  concert_scale.innerHTML = notes[new_idx];
}

function openSettings() {
  // Show/hide settings overlay
  let visibility = viewSettings.style.getPropertyValue('visibility');
  let newStyle = 'hidden';
  if (visibility == '' || visibility == 'hidden') {
    newStyle = 'visible';
  }
  viewSettings.style.setProperty('visibility', newStyle);
}

// ref: https://www.music.mcgill.ca/~gary/307/week1/node28.html
function getFreqFromMidi(midiNote) {
  // 440*2^((n-69)/12)
  let n = midiNote;
  return 440*Math.pow(2, (n-69)/12);
}


function _getId(id) {
  return document.getElementById(id);
}
function _new(el) {
  return document.createElement(el);
}
